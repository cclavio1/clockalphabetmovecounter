console.log(clockCounter("AZGB"))

function clockCounter(s){
	let letters = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
	let counter=0
	let directionIsRight= true;
	let lastLetter = "A"
	for(let x= 0;x<s.length;x++){
		let currentLetter = s[x]
		if(directionIsRight){
			for(let y = letters.indexOf(lastLetter);y<letters.length;y++){
				if(currentLetter==letters[y]){
					break
				}
				if(y>=25){
					y=-1
				}
				counter++
			}
		}else{
			for(let y = letters.indexOf(lastLetter);y<letters.length;y--){
				if(currentLetter==letters[y]){
					break
				}
				if(y<=0){
					y=26
				}
				counter++
			}
		}
		console.log(lastLetter,currentLetter,counter)
		directionIsRight = !directionIsRight
		lastLetter = currentLetter
	}

	return counter
}